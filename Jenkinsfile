#!/usr/bin/env groovy

pipeline {
    agent any

    tools {
        nodejs 'node'
    }

    stages {
        stage('Incremental version') {
            steps {
                script {
                    dir('app') {
                        sh "npm version minor"
                        def packageJson = readJSON file: 'package.json'
                        def version = packageJson.version
                        env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                    }
                }
            }
        }

        stage('Run Test') {
            steps {
                script {
                    dir('app') {
                        sh 'npm install'
                        sh 'npm run test'
                    }
                }
            }
        }

        stage('Build and Push Docker Image') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
                    sh "docker build -t blaze14/node-app:${env.IMAGE_NAME} ."
                    sh 'echo $PASS | docker login -u $USER --password-stdin'
                    sh "docker push blaze14/node-app:${env.IMAGE_NAME}"
                }
            }
        }

        stage('Commit Version Update') {
            steps {
                script {
                    sshagent(credentials: ['gilag-ssh']) {
                        sh 'git config --global user.email "jenkins@examples.com"'
                        sh 'git config --global user.name "blaz14"'
                        sh "git remote set-url origin git@gitlab.com:my-project-20231/my-jenkins-exercise.git"
                        sh 'git add .'
                        sh 'git commit -m "ci: version bump"'
                        sh 'git push origin HEAD:master'
                    }
                }
            }
        }
    }
}
