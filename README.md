## This project is about build automation CI/CD with jenkins exercise

### Dockerize my NodeJS App ##
Created Dockerfile in the root folder of the project

### Created Jenkins Credentials

Created usernamePassword credentials for docker registry called docker-hub-repo

Created sshagent credentials for git repository called gitlag-ssh

Configured Node Tool in Jenkins Configuration

### Install plugin

Installed Pipeline Utility Steps plugin. This contains readJSON function, that will be use to read the version from package.json

### Manually deploy new Docker Image on server

### Extract into Jenkins Shared Library

Created a separate git repo for Jenkins Shared Library-1

Extracted code withing the script blocks from increment version, Run tests, Build and Push docker image and commit version update to Jenkins Shared Library-1

Configured my Jenkinsfile to use the Jenkins Shared Library-1 project
